package _currentLesson;

public class Person {
    private String firstName;
    private String lastName;
    private int yearOfBirth;

    Person(String firstName, String lastName, int yearOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
    }

    int getAge() {
        return 2020 - yearOfBirth;
    }

}

class Solution {
    public static void main(String[] args) {
        Person person = new Person("Ala", "N/A", 1987);
        System.out.println(person.getAge());
    }
}
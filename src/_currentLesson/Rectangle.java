package _currentLesson;

class Rectangle {
    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getArea() {
        return width * height;
    }
    public int getArea(int width, int height) {
        return width * height;
    }
}

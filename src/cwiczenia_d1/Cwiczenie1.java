package cwiczenia_d1;

public class Cwiczenie1 {
    public static void priceList(int basePrice, int maxPrice, int maxLines) {
        for (int i = 1; i <= maxLines; i++) {
            if (maxPrice >= basePrice * i) {
                System.out.println("Liczba sztuk: " + i + " łączna cena: " + basePrice * i + " zł");
            }
        }
    }

    public static void main(String[] args) {
        priceList(5, 15, 3);
    }

}

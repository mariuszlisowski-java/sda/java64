package cwiczenia_d1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Cwiczenie10 {
    private static int MIN_PASS_CHARACTERS = 6;

    private static Pattern letter = Pattern.compile("[a-zA-Z]");
    private static Pattern digit = Pattern.compile("[0-9]");
    private static Pattern special = Pattern.compile ("[\\[\\]!@#1$%&*()_+=|<>?{}~-]");
    private static Pattern lowercase = Pattern.compile("[a-z]");
    private static Pattern uppercase = Pattern.compile("[A-Z]");
    private static Pattern space = Pattern.compile("[ ]");

    private static enum Error {
        NO_LOWERCASE,
        NO_UPPERCASE,
        NO_DIGIT,
        NO_LETTER,
        NO_SPECIAL,
        TOO_SHORT,
        HAS_SPACE,
        OK
    }

    public static Error isPasswordValid(String password) {
        if (password.length() >= MIN_PASS_CHARACTERS) {
            Matcher hasLetter = letter.matcher(password);
            Matcher hasDigit = digit.matcher(password);
            Matcher hasSpecial = special.matcher(password);
            Matcher hasLowercase = lowercase.matcher(password);
            Matcher hasUppercase = uppercase.matcher(password);
            Matcher hasSpace = space.matcher(password);

            if (hasSpace.find()) return Error.HAS_SPACE;
            if (!hasDigit.find()) return Error.NO_DIGIT;
            if (!hasLetter.find()) return Error.NO_LETTER;
            if (!hasSpecial.find()) return Error.NO_SPECIAL;
            if (!hasLowercase.find()) return Error.NO_LOWERCASE;
            if (!hasUppercase.find()) return Error.NO_UPPERCASE;

            return Error.OK;
        } else {
            return Error.TOO_SHORT;
        }
    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter password: ");
        String password = reader.readLine();

        switch (isPasswordValid(password)) {
            case TOO_SHORT:
                System.out.println("Should be at least " + MIN_PASS_CHARACTERS + " characters long!");
                break;
            case HAS_SPACE:
                System.out.println("Cannot contain spaces!");
                break;
            case NO_DIGIT:
                System.out.println("Should contain at least one digit!");
                break;
            case NO_LETTER:
                System.out.println("Should contain at least one letter!");
                break;
            case NO_SPECIAL:
                System.out.println("Should contain at least special character!");
                break;
            case NO_LOWERCASE:
                System.out.println("At least one letter should be lowercase!");
                break;
            case NO_UPPERCASE:
                System.out.println("At least one letter should be uppercase!");
                break;
            case OK:
                System.out.println("Good quality password!");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + isPasswordValid(password));
        }
    }

}

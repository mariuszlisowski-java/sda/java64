package cwiczenia_d1;

public class Cwiczenie2 {

    public static void priceList(int basePrice, int discount, int maxLines, int minItems) {
        for (int i = 1; i <= maxLines; i++) {
            double discountedPrice;
            if (i  < minItems) {
                discountedPrice = basePrice * i;
            } else {
//                System.out.println(basePrice * i);
//                System.out.println(basePrice * i * discount / 100.0);
                discountedPrice = (basePrice * i) - (basePrice * i * discount / 100.0);
            }
            System.out.println("Liczba sztuk: " + i + " łączna cena: " + discountedPrice + " zł");
        }
    }

    public static void main(String[] args) {
        priceList(5, 10, 4, 3);
    }

}

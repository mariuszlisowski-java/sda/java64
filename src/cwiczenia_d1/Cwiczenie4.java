package cwiczenia_d1;

public class Cwiczenie4 {

    public static void multiplicationTable(int x, int y) {
        if (x > 0 && y > 0) {
            for (int i = 0; i <= y; i++) {
                if (i == 0) {
                    System.out.print("   ");
                    for (int k = 1; k <= x; k++) {
                        System.out.print(k + " ");
                    }
                    System.out.println();
                }
                else {
                    System.out.print(i + "  ");

                    for (int j = 1; j <= x; j++) {
                        if ((i * j) % 2 == 0) {
                            System.out.println();
                        }
                        System.out.print(i * j + " ");
                    }
                    System.out.println();
                }
            }
        }

    }

    public static void main(String[] args) {
        multiplicationTable(3, 4);
    }

}

package cwiczenia_d1;

import java.util.Scanner;

public class Cwiczenie5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int sum = 0, idx = 0;
        while (true) {
            int value = scan.nextInt();
            if (value == 0) {
                break;
            }
            sum += value;
            ++idx;
        }
        System.out.println("Arithmetic average: " + (sum / idx));
    }

}

package cwiczenia_d1;

public class Cwiczenie6 {

    public static int[] cyclicLeftShift(int[] arr) {
        int i;

        int temp = arr[0];
        for (i = 0; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr[i] = temp;

        return arr;
    }

    public static void printArray(int[] arr) {
        for (int el : arr) {
            System.out.print(el + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        printArray(array);

        array = cyclicLeftShift(array);
        printArray(array);
    }

}

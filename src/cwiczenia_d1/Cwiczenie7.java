package cwiczenia_d1;

public class Cwiczenie7 {
    public static void main(String[] args) {
        String text = "How many vowels is in this string?";

        int count = 0;
        for (char el : text.toCharArray()) {
            switch (el) {
                case 'a':
                case 'e':
                case 'i':
                case 'u':
                case 'o':
                    ++count;
                default:
            }
        }
        System.out.println("No of vowels: " + count);
    }

}

package cwiczenia_d1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Cwiczenie9 {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter password: ");
        String password = reader.readLine();

        System.out.println(isPasswordValid(password) ?
                "Password OK" : "Not good enough");
    }

    public static boolean isPasswordValid(String password) {
        if (password.length() >= 6) {
            Pattern letter = Pattern.compile("[a-zA-Z]");
            Pattern digit = Pattern.compile("[0-9]");
            Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
            Pattern lowercase = Pattern.compile("[a-z]");
            Pattern uppercase = Pattern.compile("[A-Z]");
            Pattern hash = Pattern.compile("[#]");
            Pattern slash = Pattern.compile("[/]");
            Pattern space = Pattern.compile("[ ]");

            Matcher hasLetter = letter.matcher(password);
            Matcher hasDigit = digit.matcher(password);
            Matcher hasSpecial = special.matcher(password);
            Matcher hasLowercase = lowercase.matcher(password);
            Matcher hasUppercase = uppercase.matcher(password);
            Matcher hasHash = hash.matcher(password);
            Matcher hasSlash = slash.matcher(password);
            Matcher hasSpace = space.matcher(password);

            return !hasSpace.find() &&
                   hasSlash.find() && hasHash.find() &&
                   hasUppercase.find() && hasLowercase.find();
        } else {
            return false;
        }
    }

}

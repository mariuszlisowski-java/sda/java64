package lista1.complex;

public class Complex {
    public static int counter = 0;
    private double real;
    private double imaginary;

    public Complex(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
        counter++;
    }
    public Complex(double real) {
        this(real, 0);
    }

    public static Complex add(Complex lhs, Complex rhs) {
        double sumReal = lhs.real + rhs.real;
        double sumImaginary = lhs.imaginary + rhs.imaginary;

        return new Complex(sumReal, sumImaginary);
    }
    public static Complex add(Complex ... complexes) {
        Complex sum = new Complex(0, 0);
        for (Complex complex : complexes) {
            sum = add(sum, complex);
        }
        return sum;
    }

    public static Complex subtract(Complex lhs, Complex rhs) {
        double subReal = lhs.real - rhs.real;
        double subImaginary = lhs.imaginary - rhs.imaginary;

        return new Complex(subReal, subImaginary);
    }

    public void print() {
        if (imaginary > 0){
            System.out.println(real + "+" + imaginary + "*i");
        } else if (imaginary < 0){
            System.out.println(real + "" + imaginary + "*i");
        } else {
            System.out.println(real);
        }
    }

    public boolean isEqualTo(Complex rhs) {
        return this.real == rhs.real &&
               this.imaginary == rhs.imaginary;
    }

    public static boolean areEqual(Complex lhs, Complex rhs) {
        return lhs.real == rhs.real &&
               lhs.imaginary == rhs.imaginary;
    }

    public void increase(double real, double imaginary) {
        this.real += real;
        this.imaginary += imaginary;
    }

    public void increase(double value) {
        increase(value,value);
    }

    public void increaseReal(double value) {
        increase(value, 0);
    }

    public void increaseImaginary(double value) {
        increase(0, value);
    }


}

package lista1.complex;

import lista1.complex.Complex;

public class Main {
    public static void main(String[] args) {
        Complex c1 = new Complex(2, 3);
        Complex c2 = new Complex(5);

        Complex c3 = new Complex(-2, -2);
        Complex c4 = new Complex(-2, -2);

        c1.print();
        c2.print();
        c3.print();

        Complex sum = Complex.add(c1, c2);
        sum.print();

        System.out.println(c3.isEqualTo(c4)); // true
        System.out.println(c1.isEqualTo(c4)); // false

        System.out.println(Complex.areEqual(c3, c4)); // true
        System.out.println(Complex.areEqual(c1, c4)); // false

        Complex sumOfThree = Complex.add(c1, c2, c3);
        sumOfThree.print();

        c2.increase(1, 1);
        c2.print();

        c2.increase(-1);
        c2.print();

        System.out.println("Complex objects so far: " + Complex.counter);

    }

}

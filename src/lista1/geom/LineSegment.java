package lista1.geom;

public class LineSegment {
    private Point endpointA;
    private Point endpointB;

    public LineSegment(Point endpointA, Point endpointB) {
        this.endpointA = endpointA;
        this.endpointB = endpointB;
    }
    public LineSegment(double x1, double y1, double x2, double y2) {
        endpointA = new Point(x1, y1);
        endpointB = new Point(x2, y2);
    }

    public boolean isInOneQuadrant() {
        return endpointA.getQuadrant() == endpointB.getQuadrant();
    }

    public double getLength() {
        return Math.sqrt(Math.pow(endpointB.getX() - endpointA.getX(), 2) +
                         Math.pow(endpointB.getY() - endpointB.getY(), 2));
    }

    public void moveBeginPoint(double x, double y) {
        endpointA.setX(x);
        endpointA.setY(y);
    }
    public void moveEndPoint(double x, double y) {
        endpointB.setX(x);
        endpointB.setY(y);
    }
}

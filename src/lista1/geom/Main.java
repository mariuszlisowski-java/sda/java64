package lista1.geom;

public class Main {
    public static void main(String[] args) {
        Point point1 = new Point(3, 4);
        Point point2 = new Point(-3, 4);
        Point point3 = new Point(-3, -4);
        Point point4 = new Point(3, -4);

        System.out.println(point1.getQuadrant());
        System.out.println(point2.getQuadrant());
        System.out.println(point3.getQuadrant());
        System.out.println(point4.getQuadrant());

        System.out.println(point1);

        Point pointA = new Point(1, 2);
        Point pointB = new Point(3, 4);
        Point pointC = new Point(-3, -4);

        LineSegment lineSegment1 = new LineSegment(pointA, pointB);
        System.out.println(lineSegment1.isInOneQuadrant());

        LineSegment lineSegment2 = new LineSegment(pointA, pointC);
        System.out.println(lineSegment2.isInOneQuadrant());

        System.out.println(lineSegment1.getLength());
        System.out.println(lineSegment2.getLength());

        lineSegment1.moveBeginPoint(2, 2);
        lineSegment2.moveEndPoint(-5, -6);

        System.out.println(lineSegment1.getLength());
        System.out.println(lineSegment2.getLength());
    }
}

package lista1.geom;

public class Point {
    private double x;
    private double y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }

    public void addX(double value) {
        this.x += value;
    }
    public void addY(double value) {
        this.y += value;
    }

    public byte getQuadrant() {
        if (x > 0 && y > 0) { return 1; }
        if (x < 0 && y > 0) { return 2; }
        if (x < 0 && y < 0) { return 3; }
        if (x > 0 && y < 0) { return 4; }
        return 0;
    }

    public String toString() { return "P(" + x + "," + y + ")"; }
}

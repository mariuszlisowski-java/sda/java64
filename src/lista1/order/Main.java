package lista1.order;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // pick orders
        OrderItem ordered1 = new OrderItem("Book", 2, 10);
        OrderItem ordered2 = new OrderItem("CD", 3, 5);
        OrderItem ordered3 = new OrderItem("DVD", 1, 30);
        // get first item info
        System.out.println(ordered1.getQuantity() + " x " + ordered1.getItemName() +
                           ": each for " + ordered1.getItemPrice() + " PLN");
        ordered1.print();

        // sum odrers
        List<OrderItem> orderList = new ArrayList<OrderItem>() {{
            add(ordered1);
            add(ordered2);
        }};

        // create order
        Order order = new Order(orderList);
        // add an item to order
        order.addItem(ordered3);
        // get order info
        System.out.println("Ordered items: " + order.getItemsCount());
        order.print();
        System.out.println("Total order: " + order.getItemsValue() + " PLN");
    }

}

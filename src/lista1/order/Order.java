package lista1.order;

import java.util.List;

public class Order {
    private List<OrderItem> orderItems;

    public Order(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public double getItemsValue() {
        double orderTotal = 0;
        for (OrderItem orderItem : orderItems) {
            orderTotal += orderItem.getValue();
        }

        return orderTotal;
    }

    public int getItemsCount() {
        int count = 0;
        for(OrderItem orderItem : orderItems) {
            count += orderItem.getQuantity();
        }

        return count;
    }

    public void addItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }

    public void print() {
        for (OrderItem orderItem : orderItems) {
            System.out.println(orderItem.getItemName() + " " + orderItem.getItemPrice() + " PLN " +
                               ": total for " + orderItem.getQuantity() + " item(s): " +
                               orderItem.getValue() + " PLN");
        }
    }

}

package lista1.order;

public class OrderItem {
    private String itemName;
    private int quantity;
    private double itemPrice;

    public OrderItem(String itemName, int quantity, double itemPrice) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.itemPrice = itemPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getItemName() {
        return itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public double getValue() {
        return itemPrice * quantity;
    }

    public boolean isCorrect() {
        return itemPrice > 0 && quantity > 0;
    }

    public void print() {
        if (isCorrect()) {
            System.out.println(itemName + " " + itemPrice + " PLN " +
                    ": total for " + quantity + " item(s): " + getValue() + " PLN");
        }
    }

}

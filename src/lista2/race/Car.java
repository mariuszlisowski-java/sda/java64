package lista2.race;

public class Car {
    private final Driver driver;
    private int distance;

    Car(Driver driver) {
        this.driver = driver;
    }

    public int getDistance() {
        return distance;
    }

    public String getDriverName() {
        return driver.getName();
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}

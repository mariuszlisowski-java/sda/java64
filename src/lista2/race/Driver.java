package lista2.race;

public class Driver {
    private final String name;
    private int points;

    public Driver(String name) {
        this.name = name;
        this.points = 0;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void increasePointsBy(int points) {
        this.points += points;
    }
}

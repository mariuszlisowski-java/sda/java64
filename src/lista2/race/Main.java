package lista2.race;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Driver beginner = new Driver("Alice");
        Driver intermediate = new Driver("Rose");
        Driver advanced = new Driver("Kate");
        Driver epic = new Driver("Raisin");

        Car slow = new Car(beginner);
        Car moderate = new Car(intermediate);
        Car fast = new Car(advanced);
        Car fastest = new Car(epic);

        List<Car> raceCars = new ArrayList<>() {{
           add(slow);
           add(moderate);
           add(fast);
           add(fastest);
        }};

        Race race = new Race(3);

        race.start(raceCars);
    }
}

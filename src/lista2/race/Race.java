package lista2.race;

import java.util.*;

public class Race {
    private Car car;
    private final int rounds;
    final static int WIN_POINTS = 3;

    public Race(int rounds) {
        this.rounds = rounds;
    }

    public void start(List<Car> cars) {
        for (int i = 0; i < rounds; i++) {
            System.out.println("###########  Round " + i + "  ###########");
            int j = 0;
            for (Car car : cars) {
                car.setDistance(getRandomDistance());
                drawTrack(car);
            }
            rewardRoundWinners(cars);
            printSpacer();
        }
        rewardRaceWinners(cars);
        printSpacer();
    }

    private void printSpacer() {
        System.out.println("_________________________________");
    }

    private void rewardRaceWinners(List<Car> cars ) {
        Car raceWinner = Collections.max(cars, Comparator.comparing(s -> s.getDriver().getPoints()));
        for (Car car : cars) {
            if (raceWinner.getDriver().getPoints() == car.getDriver().getPoints()) {
                System.out.println("The winner is: " +
                                   car.getDriver().getName() +
                                   " (" + car.getDriver().getPoints() + " points)");
            }
        }
    }

    private void rewardRoundWinners(List<Car> cars) {
        Car roundWinner = Collections.max(cars, Comparator.comparing(Car::getDistance));
        for (Car car : cars) {
            if (roundWinner.getDistance() == car.getDistance()) {
                car.getDriver().increasePointsBy(WIN_POINTS);
                System.out.print(": The winner: " + car.getDriverName());
                System.out.println(" (+ " + WIN_POINTS + " points)");
            }
        }
    }

    private int getRandomDistance() {
        return new Random().nextInt(9) + 1;
    }

    private void drawTrack(Car car) {
        printDriverName(car.getDriverName());
        for (int i = 0; i < car.getDistance(); i++) {
            System.out.print(". ");
        }
        System.out.println(" @");
    }

    private void printDriverName(String str) {
        String formatted = String.format("%9s", str);
        System.out.print(formatted + " ");
    }

}

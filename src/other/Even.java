package other;

public class Even {
    public static void main(String[] args) {
        System.out.println(isEven(11) ?
                           true : false);
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }
}

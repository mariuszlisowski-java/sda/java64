package other;

import java.util.*;
import java.util.stream.Collectors;

public class Occurences {
    public static void main(String[] args) {
        Integer[] arr = {1, 1, 3, 4, 1, 4};

        ocurrences(arr, 1);
        countOccurences(arr);
        occurenceList(arr);
    }

    public static void ocurrences(Integer[] arr, int value) {
        // foreach counting
        int count = 0;
        for (int el : arr) {
            if (el == value) {
                count++;
            }
        }
        System.out.println("Occurences of 1: " + count);
    }

    public static void occurenceList(Integer[] arr) {
        List list = Arrays.asList(arr);
        Set<Integer> set = new HashSet<Integer>(list);
        for(Integer el: set){
            System.out.println(el + " " + Collections.frequency(list, el));
        }
    }



    public static void countOccurences(Integer[] arr) {
        Arrays.stream(arr)
                .collect(Collectors.groupingBy(s -> s))
                .forEach((el, count) -> System.out.println((el == 1) ? count.size() : ""));
    }

}

package other;

public class ReverseString {
    public static void main(String[] args) {
        String word = "ReversE";

        StringBuilder input = new StringBuilder();
        input.append(word);
        input = input.reverse();

        System.out.println(input);
    }

}

package other;

public class Swap {
    public static void main(String[] args) {

        int[] numbers = {1, 6, 9};
        swap(numbers, 0, 2);


        for (int el : numbers) {
            System.out.print(el + " ");
        }
        System.out.println();
    }

    // swapping without temp
    public static void swap(int[] arr, int i, int j) {
        arr[i] = (arr[i] + arr[j]) - (arr[j] = arr[i]);
    }

}

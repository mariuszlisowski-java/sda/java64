package wprawki;

//        Mamy dwie liczby A i B. A oznacza
//        dzień miesiąca, B to numer miesiąca.
//        Sprawdź, czy z tych dwóch liczb dałoby
//        się zbudować poprawną datę (dla
//        uproszczenia pomijamy lata przestępne).

public class cwiczenie6 {
    public enum Months {
        JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC
    }
    public static void main(String[] args) {
        int day = 28;
        Months month = Months.FEB;

        checkDate(day, month);
        checkDateCond(day, month);
    }

    public static void checkDateCond(int day, Months month) {
        if ((day >=1 && day <= 31 && (month == Months.JAN || month == Months.MAR ||
                                      month == Months.MAY || month == Months.JUL ||
                                      month == Months.AUG || month == Months.OCT ||
                                      month == Months.DEC)) ||
            (day >= 1 && day <= 28 && month == Months.FEB) ||
            (day >= 1 && day <= 30 && (month == Months.APR || month == Months.JUN ||
                                       month == Months.SEP || month == Months.NOV)))
        {
            System.out.println("Date OK");
        } else {
            System.out.println("No such date");
        }
    }

    public static void checkDate(int day, Months month) {
        boolean success = false;
        switch (month) {
            case JAN: case MAR: case MAY: case JUL: case AUG: case OCT: case DEC:
                if (day >= 1 && day <= 31) success = true;
                break;
            case FEB:
                if (day >= 1 && day <= 28) success = true;
                break;
            case APR: case JUN: case SEP: case NOV:
                if (day >= 1 && day <= 30)  success = true;
                break;
        }
        System.out.println(success == true ?
                "Mozna utworzyc date" :
                "Nie da sie utworzyc daty");
    }

    }

package wprawki;

//        Mamy dwie liczby A i B. A oznacza
//        cenę jednostkową towaru, B oznacza ilość
//        zakupionych sztuk. Sklep oferuje zniżkę
//        10% jeśli całkowita wartość zakupu
//        przekroczy 1000zł. Wyświetl ostateczną
//        cenę dla różnych wartości A i B.

public class cwiczenie8 {
    public static void main(String[] args) {
        discountA();
        discountB();
    }

    public static void discountA() {
        double unitPrice = 11;
        double quantity = 100;
        double discountAt = 1000;
        double discountPercent = 10;

        double totalPrice = unitPrice * quantity;
        if (totalPrice > discountAt) {
            double discountedPrice = totalPrice - (totalPrice * (discountPercent / 100));
            System.out.println("Price to pay (discounted): " + discountedPrice);
        } else {
            System.out.println("Price to pay (no discount): " + totalPrice);
        }
    }

    public static void discountB() {
        float a = 11;//cena
        int b = 100; //sztuki
        float total = a * b;
        double znizka = total * 0.10;
        if (a * b > 1000) {
            System.out.println("You have 10% discount. " + "Total: " + (total-znizka));
        } else{
            System.out.println("Total: " +total);
        }
    }
}

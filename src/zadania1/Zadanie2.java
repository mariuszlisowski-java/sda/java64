package zadania1;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        String text = " This is an example text for testing testing";
        ArrayList<String> longestWords = new ArrayList<>();
        Scanner words = new Scanner(text);
        int max = Integer.MIN_VALUE;

        // only first longest word
        String longestWord = "";
        while (words.hasNext()) {
            String word = words.next();
            if (word.length() > max) {
                longestWord = word;
                max = word.length();
            }
        }
        System.out.println(longestWord);

        // all longest words
        words = new Scanner(text);
        while (words.hasNext()) {
            String word = words.next();
            if (longestWord.length() == word.length()) {
                longestWords.add(word);
            }
        }
        for (String el : longestWords) {
            System.out.print(el + " ");
        }
        System.out.println();

        words.close();
    }
}

package zadania1;

import java.util.Arrays;

public class Zadanie3 {
    public static void main(String[] args) {
        int arithSeqLength = 50;
        int arithSeqFirst = 5;
        int arithSeqDiff = 4;

        for (int i = arithSeqFirst; i < arithSeqLength; i += arithSeqDiff) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

}

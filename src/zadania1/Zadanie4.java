package zadania1;

import java.util.Arrays;

public class Zadanie4 {
    public static void main(String[] args) {
        int[] arr = {15, 1, 3, 5, 7, 9, 11, 13};
        Arrays.sort(arr);
        System.out.println(isArithmeticSequence(arr) ?
                "Arithmetic sequence" : "Not Arithmetic sequence");
    }

    public static boolean isArithmeticSequence(int [] nums) {
        int diffArith = 0;
        boolean arithFlag = true;
        diffArith = nums[1] - nums[0];
        for (int y = 0; y < nums.length - 1 && arithFlag; y++) {
            arithFlag = false;
            if (nums[y] + diffArith == nums[y + 1]) {
                arithFlag = true;
            }
        }
        return arithFlag ? true : false;
    }

}



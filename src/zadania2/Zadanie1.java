package zadania2;

public class Zadanie1 {
    public static void main(String[] args) {
        System.out.println(modulo(10, 3));
    }

    public static int modulo(int number, int divisor) {
        return (number - divisor * (number / divisor));
    }
}

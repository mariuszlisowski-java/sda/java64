package zadania2;

public class Zadanie2 {
    public static void main(String[] args) {
        System.out.println(modulo1(100, 7));
        System.out.println(modulo2(100, 7));
    }

    public static int modulo1(int number, int divisor) {
        return (number - divisor * (number / divisor));
    }

    public static int modulo2(int number, int divisor) {
        return number % divisor;
    }

}

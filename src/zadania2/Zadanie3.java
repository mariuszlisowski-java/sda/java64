package zadania2;

public class Zadanie3 {
    public static void main(String[] args) {
        System.out.println(
            palindrome("Kobyła ma mały bok") ?
            "Palindrome" : "NOT palindrome");
    }

    public static boolean palindrome(String sentence) {
        sentence = sentence.replace(" ", "");
        sentence = sentence.toLowerCase();

        StringBuilder input = new StringBuilder(sentence);
        input = input.reverse();

        return sentence.equals(input.toString());
    }
}

package zadania2;

public class Zadanie4 {
    public static int power(int base, int exponent) {
        return exponent != 0 ?
               base * power(base, exponent - 1) : 1;
    }

    public static void main(String[] args) {
        System.out.println(power(2, 4));
    }
}

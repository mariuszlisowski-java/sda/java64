package zadania3;

import java.util.stream.Stream;

public class Zadanie1 {
    public static void main(String[] args) {
        triangle(5);
    }

    static int n = 1;
    public static void triangle(int val) {
        int count = val - (val - n++);
        if (val > 0) {
            Stream.generate(() -> "*").limit(count).forEach(System.out::print);
            System.out.println();
            triangle(val - 1);
        }
        count -= 2;
        if (count > 0) {
            Stream.generate(() -> "*").limit(count).forEach(System.out::print);
            System.out.println();
        }
    }

}

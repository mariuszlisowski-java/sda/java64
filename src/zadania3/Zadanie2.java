package zadania3;

public class Zadanie2 {
    public static void main(String[] args) {
        int test1 = 2147483646;
        int test2 = 2147483647;

        System.out.println(isPrime(test1) ? "Prime" : "Not prime");
        System.out.println(isPrime(test2) ? "Prime" : "Not prime");
    }

    public static boolean isPrime(int n) {
        if( n == 2 ) {
            return true;
        } else if (n == 1 || (n & 1) == 0) {
            return false;
        }
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true; // n is prime
    }

}

package zadania3;

public class Zadanie3 {
    public static void main(String[] args) {
        int COUNT = 4;

        long sum = sumPrimeNumbers(COUNT);

        System.out.println("Sum of least " + COUNT + " prime numbers: " + sum);
    }

    public static long sumPrimeNumbers(int count) {
        int index = 1;
        long sum = 0;
        while (count > 0) {
            if (isPrime(index)) {
                --count;
                sum += index;
            }
            ++index;
        }

        return sum;
    }

    public static boolean isPrime(int n) {
        if( n == 2 ) {
            return true;
        } else if (n == 1 || (n & 1) == 0) {
            return false;
        }
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true; // n is prime
    }

}

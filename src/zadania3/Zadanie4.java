package zadania3;

import java.util.Calendar;
import java.util.Date;

public class Zadanie4 {
    static int DAY = 33;

    public static void main(String[] args) {

        System.out.println("Day of year " + DAY + ": " +
                            dayOfTheYear(DAY));
    }

    public static Date dayOfTheYear(int dayOfYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);

        return calendar.getTime();
    }

}

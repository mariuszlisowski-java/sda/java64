package zadania4;

public class Zadanie1a {
    public static String encode(String input) {
        String encoded = "";
        for (char el : input.toCharArray()) {
            el = Character.toUpperCase(el);
            switch (el) {
                case 'A':
                    encoded += '4';
                    break;
                case 'E':
                    encoded += '3';
                    break;
                case 'I':
                    encoded += '1';
                    break;
                case 'O':
                    encoded += '0';
                    break;
                case 'S':
                    encoded += '$';
                    break;
                default:
                    encoded += el;
            }
        }
        return encoded;
    }
    public static String decode(String input) {
        String decoded = "";
        for (char el : input.toCharArray()) {
            el = Character.toUpperCase(el);
            switch (el) {
                case '4':
                    decoded += 'A';
                    break;
                case '3':
                    decoded += 'E';
                    break;
                case '1':
                    decoded += 'I';
                    break;
                case '0':
                    decoded += 'O';
                    break;
                case '$':
                    decoded += 'S';
                    break;
                default:
                    decoded += el;
            }
        }
        return decoded;
    }

    public static void main(String[] args) {
        String input = "Asia";

        String encoded = encode(input);
        String decoded = decode(encoded);

        System.out.println(encoded);
        System.out.println(decoded);
    }

}

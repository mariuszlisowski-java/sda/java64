package zadania4;

import java.lang.reflect.GenericDeclaration;
import java.util.regex.Pattern;

public class Zadanie1b {
    public static void main(String[] args) {
//        String input = "asia";
        String input = "Asia";

        String encoded = encode(input);
        String decoded = decode(encoded);

        System.out.println(encoded);
        System.out.println(decoded);
    }
    public static char[] naturalLetters = {'a', 'e', 'i', 'o', 's'};
    public static char[] leetLetters =    {'4', '3', '1', '0', '$'};

    public static String encode(String input) {
        input = input.toLowerCase();
        for (int i = 0; i < naturalLetters.length; i++) {
            input = input.replace(naturalLetters[i], leetLetters[i]);
        }

        return input;
    }

    public static String decode(String input) {
        for (int i = 0; i < naturalLetters.length; i++) {
            input = input.replace(leetLetters[i], naturalLetters[i]);
        }

        return input;
    }

}

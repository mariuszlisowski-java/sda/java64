package zadania4;

import java.beans.PropertyEditorSupport;

public class Zadanie3 {
    public static void fibonacciSeries(int count) {
        if (count < 0) {
            return;
        }

        int sum = 0, prevA = 0, prevB = 1;
        System.out.print(prevA + " " + prevB);
        for (int i = 2; i < count; i++) {
            sum = prevA + prevB;
            prevA = prevB;
            prevB = sum;
            System.out.print(" " + sum);
        }
    }

    public static void main(String[] args) {
        fibonacciSeries(20);
    }
}

package zadania4;

public class Zadanie4 {
    public static void main(String[] args) {
        peselChecker();
    }

    public static void peselChecker() {
        String PESEL = "77063017739";
        int[] pesel = new int[PESEL.length()];

        for (int i = 0; i < PESEL.length(); i++) {
            pesel[i] = PESEL.charAt(i) - 48; // zamiana tekstu na tablicę cyfr
        }

        for (int el : pesel) {
            System.out.print(el);
        }
        System.out.print(" - ");

        /* reguła zgodności numeru PESEL (suma kończy się cyfrą zero) */
        int suma = pesel[0]*1 + pesel[1]*3 + pesel[2]*7 + pesel[3]*9 +pesel[4]*1 +
                   pesel[5]*3 + pesel[6]*7 + pesel[7]*9 + pesel[8]*1 + pesel[9]*3 + pesel[10]*1;

        if (suma > 0) {
            if (suma % 10 == 0) {
                System.out.println("OK");// czy ostatnia cyfra jest zerem?
            } else {
                System.out.println("Incorrect");
            }
        }

    }
}

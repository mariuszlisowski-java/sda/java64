package zadania4;

public class Zadanie5 {
    public static void main(String[] args) {
        String PESEL = "77063017739";
        try {
            peselChecker(PESEL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void peselChecker(String PESEL) throws Exception {
        if (PESEL.length() != 11) {
            throw new IllegalArgumentException("Incorrect length!");
        }
        int[] pesel = new int[PESEL.length()];

        for (int i = 0; i < PESEL.length(); i++) {
            pesel[i] = PESEL.charAt(i) - 48; // zamiana tekstu na tablicę cyfr
        }

        for (int el : pesel) {
            System.out.print(el);
        }
        System.out.print(" - ");

        /* tablica wag */
        int[] weightTable = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};

        /* reguła zgodności numeru PESEL (suma kończy się cyfrą zero) */
        int sum = 0;
        for (int i = 0; i < PESEL.length(); i++) {
            sum += pesel[i] * weightTable[i];
        }
        if (sum > 0) {
            if (sum % 10 == 0) {
                System.out.println("OK"); // czy ostatnia cyfra jest zerem?
            } else {
                System.out.println("Incorrect");
            }
        }
    }

}

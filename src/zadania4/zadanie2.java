package zadania4;

import java.util.HashMap;
import java.util.Map;

public class zadanie2 {
    public static void main(String[] args) {
        String input = "Ala i Ola mają koty i psy oraz węże !!!!!!!!";
        wordLengthAndCount(input);
    }

    public static void wordLengthAndCount(String input) {
        Map<Integer, Integer> wordLengthCount = new HashMap<>();

        String[] words = input.split(" ");
        for (String word : words) {
            int count = wordLengthCount.containsKey(word.length()) ?
                        wordLengthCount.get(word.length()) : 0;
            wordLengthCount.put(word.length(), count + 1);
        }
            
        for (Map.Entry<Integer, Integer> pair : wordLengthCount.entrySet()) {
            int key = pair.getKey();
            int value = pair.getValue();
            System.out.println("word(s) of " + key + " letter(s) long: " + value);
        }

    }

}

package zadania5;

import java.util.stream.IntStream;

public class Zadanie1 {
    public static int[] removeDuplicates(int[] array) {
        int[] uniques = IntStream.of(array).distinct().toArray();

        return uniques;
    }

    public static void nonRepeating(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for(int j = 0; j < array.length; j++) {
                if (array[i] == array[j] && i != j ) {
                    break;
                }
                if (i == j) {
                    System.out.println(array[i]);
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] numbers = {2, 5, 1, 2, 5, -1, -1};

        nonRepeating(numbers);

        System.out.println("----");

        int[] uniqueNumbers = removeDuplicates(numbers);
        for (Integer el : uniqueNumbers) {
            System.out.println(el);
        }
    }

}

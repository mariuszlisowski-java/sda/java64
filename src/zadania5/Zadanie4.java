package zadania5;

public class Zadanie4 {
    public static int binaryToDecimal(String binary) {
        binary = new StringBuilder(binary).reverse().toString();

        int decimal = 0;
        int n = 0;
        for (char ch : binary.toCharArray()) {
            if (ch == '1') {
                decimal += Math.pow(2, n);
            }
            n++;
        }

        return decimal;
    }

    public static void main(String[] args) {
        String s1 = "11111110";
        String s2 = "11111111";

        System.out.println(binaryToDecimal(s1));
        System.out.println(binaryToDecimal(s2));
    }
}

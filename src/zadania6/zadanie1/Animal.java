package zadania6.zadanie1;

public interface Animal {
    int getInstances();
    String makeSound();
}

package zadania6.zadanie1;

import zadania6.zadanie1.Animal;

public class Cat implements Animal {
    public static int count = 0;
    Cat() {
        count++;
    }
    public int getInstances() {
        return count;
    }
    public String makeSound() {
        return "miau";
    }
}

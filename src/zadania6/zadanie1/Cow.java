package zadania6.zadanie1;

public class Cow implements Animal {
    public static int count = 0;
    Cow() {
        count++;
    }
    public int getInstances() {
        return count;
    }
    public String makeSound() {
        return "muu";
    }
}

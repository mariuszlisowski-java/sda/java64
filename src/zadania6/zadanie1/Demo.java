package zadania6.zadanie1;

public class Demo {
    public static void main(String[] args) {
        Animal[] animals = new Animal[3];

        animals[0] = new Dog();
        animals[1] = new Cat();
        animals[2] = new Cow();

        for (Animal ob : animals) {
            System.out.println(ob.makeSound() + " : " + ob.getInstances());
        }

    }
}

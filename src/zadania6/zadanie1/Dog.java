package zadania6.zadanie1;

import zadania6.zadanie1.Animal;

public class Dog implements Animal {
    public static int count = 0;
    Dog() {
        count++;
    }
    public int getInstances() {
        return count;
    }
    public String makeSound() {
        return "hau";
    }
}

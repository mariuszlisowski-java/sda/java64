package zadania6.zadanie2;

public class Main {
    public static void main(String[] args) {
        Address address = new Address("Los Angeles", "View Av.", 234, 7);
        Person person = new Person("Andy", address);

        Person customer = new Person("Ian", "San Francisco", "Mountain View", 567);

        System.out.println(person.getName() + "'s address:\n" + person.getAddress());
        System.out.println(customer.getName() + "'s address:\n" + customer.getAddress());
    }

}

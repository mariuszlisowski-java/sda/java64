package zadania6.zadanie2;

public class Person  {
    private Address address;
    private final String name;

    public Person(String name, Address address) {
        this.name = name;
        this.address = address;
    }
    public Person(String name, String city, String street, int house) {
        this.name = name;
        address = new Address(city, street, house);
    }
    public Person(String name, String city, String street, int house, int flat) {
        this.name = name;
        address = new Address(city, street, house, flat);
    }

    public String getName() {
        return name;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(String city, String street, int house) {
        address = new Address(city, street, house);
    }
    public void setAddress(String city, String street, int house, int flat) {
        address = new Address(city, street, house, flat);
    }

}

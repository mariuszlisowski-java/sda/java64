package zadania6.zadanie3;

public class Address {
    private String city;
    private String street;
    private int house;
    private int flat;

    public Address(String city, String street, int house) {
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = 0;
    }
    public Address(String city, String street, int house, int flat) {
        this(city, street, house);
        this.flat = flat;
    }

    public String toString() {
        String output = "City: " + city + "\n" +
                "Street: " + street + "\n" +
                "House: " + house + "\n";
        if (flat != 0) {
            output += "Flat: " + flat + "\n";
        }

        return output;
    }

}

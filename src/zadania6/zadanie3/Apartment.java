package zadania6.zadanie3;

public class Apartment {
    private Person owner;
    private Address address;
    private Space[] spaces;
    private int rooms;

    public Apartment(Person owner, Address address, Space[] space) {
        this.owner = owner;
        this.address = address;
        this.spaces = space;
    }

    public void printInfo() {
        System.out.println("\nOwner: " + owner.getName() +
                           "\nAddress:\n" + address +
                           "Rooms: " + spaces.length + "\n");
        for (Space space : spaces) {
            System.out.println(space);
        }
    }

}

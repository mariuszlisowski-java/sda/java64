package zadania6.zadanie3;

public class Kitchen extends Space {
    private boolean isOpen;

    public Kitchen(float area, boolean isOpen) {
        super(area);
        this.isOpen = isOpen;
    }

    public String toString() {
        return "Kitchen area: " + area + " sq meters\n" +
                "Kitchen is " + (isOpen ? "open" : "closed");
    }

}

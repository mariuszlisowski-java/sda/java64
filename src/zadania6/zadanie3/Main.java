package zadania6.zadanie3;

public class Main {
    public static void main(String[] args) {
        Address flatAddress = new Address("Krakow", "Jagiellonska", 7, 13);

        Person owner = new Person("Mariusz", "Kielce", "Zagorska", 71, 12);

        Space[] spaces = {
            new Room(12, 1),
            new Room(25, 2),
            new Kitchen(10, true)
        };

        Apartment flatNo13 = new Apartment(owner, flatAddress, spaces);
        flatNo13.printInfo();
    }
}

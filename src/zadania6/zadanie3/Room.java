package zadania6.zadanie3;

public class Room extends Space {
    private int personCapacity;

    public Room(float area, int capacity) {
        super(area);
        this.personCapacity = capacity;
    }

    public String toString() {
        return "Room area: " + area + " sq meters\n" +
               "Room for " + personCapacity + " person(s)\n";
    }

}
